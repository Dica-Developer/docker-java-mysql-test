A docker image for running java tests against a MySQL, e.g. via Maven, Gradle or any other build tool.

Java 8 and 11 will be installed and MySQL with a default user root and password root. Access for the root user is allowed for host 127.0.0.1. Don't use that in production but only in an environment where this setup is no security risk.

Change the default Java version:

* to Java  8: `sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java`
* to Java 11: `sudo update-alternatives --set java /usr/lib/jvm/java-11-openjdk-amd64/bin/java`

Additionaly installed tools:

* git
* xvfb
* nodejs
* gnupg2
* ca-certificates
* libgtk2.0-0
* libnotify-dev
* libgconf-2-4
* libnss3
* libxss1
* libasound2
* yarn
* ssh
