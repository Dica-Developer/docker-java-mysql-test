#FROM phusion/baseimage:0.11
FROM ubuntu:20.10

MAINTAINER Martin Schaaf

# this is a non-interactive automated build - avoid some warning messages
ENV DEBIAN_FRONTEND noninteractive

RUN rm -f /etc/service/sshd/down

CMD ["/sbin/my_init"]

RUN apt-get update

RUN apt-get install -y ubuntu-minimal
RUN apt-get remove -y ubuntu-desktop
RUN apt-get remove -y ubuntu-server
RUN apt-get upgrade -y

# install necessary tools for a test environment
RUN apt-get install openjdk-8-jdk-headless openjdk-11-jdk-headless openjdk-15-jdk-headless xvfb  nodejs gnupg2 ca-certificates libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 -y

RUN apt-key adv -v --fetch-keys https://dl.yarnpkg.com/debian/pubkey.gpg
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update

RUN apt-get install -y yarn
RUN apt-get install -y git
RUN apt-get install -y openssh-client
RUN apt-get install -y openssh-server
RUN apt-get install -y libnss-resolve
RUN apt-get install -y nodejs

# install mysql
RUN { \
    echo mysql-community-server mysql-community-server/data-dir select ''; \
    echo mysql-community-server mysql-community-server/root-pass password ''; \
    echo mysql-community-server mysql-community-server/re-root-pass password ''; \
    echo mysql-community-server mysql-community-server/remove-test-db select true; \
  } | debconf-set-selections \
  && apt-get install mysql-server -y

RUN service mysql restart ; echo "update user set Host = '127.0.0.1', plugin ='mysql_native_password', authentication_string = '*81F5E21E35407D884A6CD4A731AEBFB6AF209E1B' where User = 'root'" | mysql -u root mysql ; service mysql stop

# clean apt
RUN apt-get -y autoremove
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

